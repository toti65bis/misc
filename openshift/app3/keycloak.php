<?php
header('Content-Type: application/json');

$config = array(
    'realm' => 'Google_SSO',
    'auth-server-url' => $_ENV["KEYCLOAK_URL"],
    'resource' => "app3"
);

echo json_encode($config, JSON_UNESCAPED_SLASHES);
?>


